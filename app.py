from flask import Flask, render_template, request, jsonify
from flask_jwt import JWT, jwt_required
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
import logging
import secrets

app = Flask(__name__)
app.config['SECRET_KEY'] = secrets.token_hex(16)  # 16 bytes for a hex-encoded key
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///tasks.db'  # Use SQLite for simplicity
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Initialize extensions
db = SQLAlchemy(app)

# Task Model
class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(200), nullable=False)

# Sample user for JWT authentication
class User:
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

users = [User(1, 'user', generate_password_hash('password'))]

# Logging setup
logging.basicConfig(filename='app.log', level=logging.DEBUG)

# JWT Configuration
def authenticate(username, password):
    user = next((user for user in users if user.username == username), None)
    if user and check_password_hash(user.password, password):
        return user

def identity(payload):
    user_id = payload['identity']
    return next((user for user in users if user.id == user_id), None)

jwt = JWT(app, authenticate, identity)

# Routes
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/tasks', methods=['GET'])
@jwt_required()
def get_tasks():
    tasks = Task.query.all()
    return jsonify({'tasks': [{'id': task.id, 'description': task.description} for task in tasks]})

# Add, edit, and delete routes go here

if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)

